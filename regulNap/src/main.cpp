/*********
repNap is an stupid open source project of room temperature regulation
*********/
#include "Arduino.h"
#include <DallasTemperature.h>


#define ONE_WIRE_BUS  4     // Data wire is plugged TO GPIO 4 (D2)
#define HEATER_PIN        5     // Heater relay is plugged TO GPIO 5 (D1)

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

// Number of temperature devices found
int numberOfDevices;

// We'll use this variable to store a found device address
DeviceAddress napAddress;
DeviceAddress roomAddress; 

uint8 napNumber = 0;
uint8 roomNumber = 1;

// Temperature variables
float napTemp;
float roomTemp;

// Temperature reference
float refTemp= 24.0;

// HEATER FSM
typedef enum {REG_OFF, REG_ON} HEATER;
HEATER heater = REG_OFF;

// function to print a device address
void printAddress(DeviceAddress deviceAddress) {
  for (uint8_t i = 0; i < 8; i++){
    if (deviceAddress[i] < 16) Serial.print("0");
      Serial.print(deviceAddress[i], HEX);
  }
}

void setup(){
  // Pin mode
  pinMode(HEATER_PIN, OUTPUT);    // the heater is an output

  // start serial port
  Serial.begin(115200);
  
  // Start up the library
  sensors.begin();
    
  // locate devices on the bus
  Serial.print("Locating devices...");
  Serial.print("Found ");
  Serial.print(numberOfDevices, DEC);
  Serial.println(" devices.");

 // Print address for each sensors
  sensors.getAddress(napAddress, napNumber);
  Serial.print("Found  nap sensor with address: ");
  printAddress(napAddress);
  Serial.println();

  sensors.getAddress(roomAddress, roomNumber);
  Serial.print("Found  room sensor with address: ");
  printAddress(napAddress);
  Serial.println();
}

void loop(){

  /////////////////////////////////////////////////
  // Get temperatures in the ground and in the room
  sensors.requestTemperatures(); // Send the command to get temperatures
  
  // get ground temp
  Serial.print("Ground temperature ");
  napTemp = sensors.getTempC(napAddress);
  Serial.print(napTemp);
  Serial.println(" °C");

  // get room temp
  Serial.print("Room temperature ");
  roomTemp = sensors.getTempC(roomAddress);
  Serial.print(roomTemp);
  Serial.println(" °C");

 //////////////////////////////////////////////////
 // HEATER FSM
 if((napTemp > (refTemp + 2)) || (roomTemp > (refTemp))) heater = REG_OFF;  // temperature is perfect to grow tomatoes
 else if(napTemp < (refTemp - 2)) heater = REG_ON;                          // It's needed to warm up the ground!

 // Set the relay state
 digitalWrite(HEATER_PIN, heater);

 // print the heater state
 if(heater) Serial.println("Heater is ON!");
 else Serial.println("Heater is OFF, temperature is allready good!");
  

  delay(5000);
}

